import * as chai from 'chai';
import chaiHttp = require('chai-http');
import app from './server';

chai.use(chaiHttp);
const expect = chai.expect;

const url = 'http://localhost:5080/';

const chaiRequest = () => chai.request(app);

describe('Test epxress', () => {
	it('Page with default layout', async () => {
		await chaiRequest().get('/defaultlayout').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body><div>Contextless page</div></body></html>');
		});
	});
	it('Page with disabled layout', async () => {
		await chaiRequest().get('/nolayout').send().then((res) => {
			expect(res.text).to.equal('<div>Contextless page</div>');
		});
	});
	it('Page with context', async () => {
		await chaiRequest().get('/context').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body><div>Welcome to the page! I am Mister of Strings!</div></body></html>');
		});
	});
	it('Page with overriden layout', async () => {
		await chaiRequest().get('/overridenlayout').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body>This is overriden layout<div>Welcome to the page! I am Mister of Strings!</div></body></html>');
		});
	});
	it('Page from subfolder', async () => {
		await chaiRequest().get('/subfolderpage').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body><div>Subfolder page</div></body></html>');
		});
	});
	it('Class page', async () => {
		await chaiRequest().get('/classpage').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body><div>Class page </div></body></html>');
		});
	});
	it('Async class page', async () => {
		await chaiRequest().get('/asyncclasspage').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body><div>Class page from promise</div></body></html>');
		});
	});
	it('Complex page', async () => {
		await chaiRequest().get('/complexpage').send().then((res) => {
			expect(res.text).to.equal('<html><head></head><body><div>Multiple components will be imported<div>Class page In wrap<div>Contextless page</div></div></div></body></html>');
		});
	});
});

