import * as express from 'express';
import pragmaticView from '../src/index'
import * as path from 'path'

const app = express()
const port = 5080;

app.set('views', './tests/templates');

pragmaticView(app, {
	templateDir: path.resolve(__dirname, './templates'),
	defaultLayout: path.resolve(__dirname, './templates/layout')
});

// tell start-server-and-test that its ready
app.head('/', (req, res) => {
	res.send(200);
});

app.get('/defaultlayout', (req,res) => {
	res.render('page');
});
app.get('/nolayout', (req,res) => {
	res.render('page', {
		layout: false
	});
});
app.get('/context', (req,res) => {
	res.render('pagecontext', {
		context: { fullName: 'Mister of Strings' } 
	});
});
app.get('/overridenlayout', (req,res) => {
	res.render('pagecontext', {
		layout: 'override-layout',
		context: { fullName: 'Mister of Strings' } 
	});
});
app.get('/subfolderpage', (req,res) => {
	res.render('subfolder/page');
});
app.get('/classpage', (req,res) => {
	res.render('classpage');
});
app.get('/asyncclasspage', (req,res) => {
	res.render('asyncclasspage');
});
app.get('/complexpage', (req,res) => {
	res.render('root');
});



app.listen(port, () => console.log(`Example app listening on port ${port}!`));

export default app;