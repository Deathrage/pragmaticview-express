import { ViewEngine } from 'pragmatic-view';
import * as Express from 'express';
import { RegisterConfig } from 'pragmatic-view/dist/register';
import * as path from 'path';

export interface PVConfig {
	viewEngine: ViewEngine,
	defaultLayout: string,
	templateExtension: string,
	templateDir: string,
	registerOptions: RegisterConfig
};

export interface RenderObject {
	[key: string]: any;
	layout?: string|boolean;
	context?: Object;
}

const flattenOptions = (options: RenderObject) => {
	if (typeof options.context === 'object') {
		Object.assign(options, options.context);
		delete options.context;
	}
	return options;
}

const getComponent = (path: string) => {
	let resolutions = require(path);
	return resolutions.default || resolutions;
}

const renderFactory = (viewEngine: ViewEngine, templatePath: string, defaultLayout: string) => (filePath: string, options: RenderObject, callback: (err:any, html:string)=>void) => {

	let layoutLoc = (options.layout || defaultLayout) as string;
	let layout = null;
	try {
		// Get Components
		if (layoutLoc && options.layout !== false) { 
			if (!path.isAbsolute(layoutLoc)) layoutLoc = path.resolve(templatePath, layoutLoc);
			layout = getComponent(layoutLoc); 
		}
		let component = getComponent(filePath);

		// Flatten context
		let context = flattenOptions(options);

		// get builder and set layout
		let viewBuilder = viewEngine.getBuilder(component, context);
		if (layout) viewBuilder.layout = layout;

		// render templates
		viewBuilder.toString().then(html => {
			callback(null, html);
		});
	} catch (err) {
		callback(err, null);
	}
}

export default function(app: Express.Express, config?: Partial<PVConfig>) {
	let clone = Object.assign({}, config || {});
	// set default extension
	if (typeof clone.templateExtension !== 'string' || !clone.templateExtension) clone.templateExtension = '.pv';
 
	// Set template path if passed
	if (clone.templateDir) {
		let relative = path.relative(process.cwd(), clone.templateDir);
		app.set('views', relative)
	}

	// Prepare view engine
	let templateDir = path.resolve(process.cwd(), app.get('views'));
	ViewEngine.register(templateDir, clone.registerOptions);
	let viewEngine = config.viewEngine || new ViewEngine();
	
	// Hook pragmatic view to express
	app.engine(clone.templateExtension, renderFactory(viewEngine, templateDir, config.defaultLayout));
	app.set('view engine', clone.templateExtension);
};
