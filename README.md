
[![npm](https://img.shields.io/npm/v/pragmatic-view-express.svg)](https://www.npmjs.com/package/pragmatic-view-express)
[![node](https://img.shields.io/node/v/pragmatic-view-express.svg)](https://nodejs.org/en/)
[![Coverage Status](https://coveralls.io/repos/gitlab/Deathrage/pragmaticview-express/badge.svg?branch=master)](https://coveralls.io/gitlab/Deathrage/pragmaticview-express?branch=master)
[![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/Deathrage/pragmaticview-express/master.svg)](https://gitlab.com/Deathrage/pragmaticview-express)

<img src="http://lukasprochazka.net/img/express.png" alt="PragmaticView" width="250" />

# PragmaticView Express

[PragmaticView](https://www.npmjs.com/package/pragmatic-view) plugin for [Express](https://expressjs.com/).

```
npm install --save pragmatic-view pragmatic-view-express
```

**Typed for Typescript**

## Getting started

[Documentation](http://pragmaticview.lukasprochazka.net/#/pages/express)

Adding custom templating engine to Express is quite inconvenient. That's why PragmaticView offers helper method that takes care of everything. Just pass express app to the helper method.

```
// ES6
import pragmaticView from 'pragmatic-view-express';
// CommonJS
const pragmaticView = require('pragmatic-view-express').default;

// creating express app
const app = express();

pragmaticView(app);

app.get('/', (req, res) => {
	res.render('home');
});

app.listen(8080);
```

You may want to pass config as additional argument. 

Config:
- `templateExtension` extension of template files (`.jsx`, `.tsx` or `.pv`), default `.pv`
- `templateDir` directory of templates (automaticaly sets `app.set('views', './directory')`),
- `defaultLayout` path to default layout template, relative is evalutaed accordint to `templateDir`
- `registerOptions` additional template transpilation options, [learn more](http://pragmaticview.lukasprochazka.net/#/pages/no-transpil?id=advanced-usage)

```
pragmaticView(app, {
	templateDir: path.resolve(process.cwd(), './views'),
	templateExtension: '.jsx'
});
```

## Rendering templates

Templates are rendered through express' `response.render` method that accept two argument. First argument is relative path to the template without extension (realtive to Express' template directory set either in `templateDir` or `app.set('views', './directory')`). Second argument is either `context` object or `options` object.

```
app.get('/aboutus', (req, res) => {
	let context = {
		title: 'My page'
	}
	res.render('home', context);
});
```

`options` object can include
- `layout` relative path (works like path to template), if `false` is passed instead of string, no layout is used
- `context` context object

```
// Rednering with different layout
app.get('/aboutus', (req, res) => {
	let context = {
		title: 'My page'
	}
	res.render('home', {
		layout: 'bluelayout',
		context: {
			title: 'My page'
		}
	});
});

// Rednering without layout
app.get('/aboutus', (req, res) => {
	let context = {
		title: 'My page'
	}
	res.render('home', {
		layout: false,
		context: {
			title: 'My page'
		}
	});
});
```